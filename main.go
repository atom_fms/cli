package main

import (
	"admin/src/adapters/handler"
	"github.com/urfave/cli"
	"log"
	"os"
)

var (
	cliHandler *handler.CLIHandler
)

func main() {
	InitCLI()
}

func InitCLI() {
	app := cli.NewApp()
	cliHandler = handler.NewCLIHandler()

	app.Commands = []cli.Command{
		{
			Name:   "addCar",
			Action: cliHandler.AddCar,
			Flags: []cli.Flag{
				&cli.StringFlag{
					Name: "plate",
				},
				&cli.StringFlag{
					Name: "model",
				},
				&cli.StringFlag{
					Name: "type",
				},
				&cli.IntFlag{
					Name:  "year",
					Value: 2023,
				},
				&cli.IntFlag{
					Name:  "mileage",
					Value: 0,
				},
			},
		},
		{
			Name:   "delCar",
			Action: cliHandler.DelCar,
			Flags: []cli.Flag{
				&cli.StringFlag{
					Name: "plate",
				},
			},
		},
		{
			Name:   "updateCar",
			Action: cliHandler.UpdateCar,
			Flags: []cli.Flag{
				&cli.StringFlag{
					Name: "plate",
				},
				&cli.StringFlag{
					Name: "model",
				},
				&cli.StringFlag{
					Name: "type",
				},
				&cli.IntFlag{
					Name: "year",
				},
			},
		},
		{
			Name:   "getCars",
			Action: cliHandler.GetCars,
		},
		{
			Name:   "getCar",
			Action: cliHandler.GetCar,
			Flags: []cli.Flag{
				&cli.StringFlag{
					Name: "plate",
				},
			},
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}
