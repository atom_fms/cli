package handler

import (
	"encoding/json"
	"fmt"
	"github.com/urfave/cli"
	"log"
	"net"
	"strconv"
)

type CLIHandler struct{}

func NewCLIHandler() *CLIHandler {
	return &CLIHandler{}
}

type Data struct {
	Command string                 `json:"command"`
	Args    map[string]interface{} `json:"args"`
}

func (c *CLIHandler) sendData(data Data) error {
	serv := "localhost:8081"
	conn, _ := net.Dial("tcp", serv)
	defer conn.Close()

	jsonData, jsonErr := json.Marshal(data)
	if jsonErr != nil {
		return jsonErr
	}

	_, err := conn.Write(jsonData)
	if err != nil {
		return err
	}

	buff := make([]byte, 1024)
	n, _ := conn.Read(buff)
	log.Printf("%s", buff[:n])

	return nil
}

func (c *CLIHandler) AddCar(ctx *cli.Context) error {
	var data Data
	data.Command = "addCar"
	//fmt.Println(ctx.Int("year"))
	//fmt.Println(rune(ctx.Int("year")))
	//fmt.Println(string(rune(ctx.Int("year"))))
	data.Args = map[string]interface{}{
		"plate":   ctx.String("plate"),
		"model":   ctx.String("model"),
		"year":    strconv.Itoa(ctx.Int("year")),
		"mileage": strconv.Itoa(ctx.Int("mileage")),
		"type":    ctx.String("type"),
	}

	err := c.sendData(data)

	if err != nil {
		return err
	}
	return nil
}

func (c *CLIHandler) DelCar(ctx *cli.Context) error {
	var data Data
	data.Command = "delCar"
	data.Args = map[string]interface{}{
		"plate": ctx.String("plate"),
	}

	err := c.sendData(data)

	if err != nil {
		return err
	}
	return nil
}

func (c *CLIHandler) UpdateCar(ctx *cli.Context) error {
	var data Data
	data.Command = "updateCar"
	data.Args = map[string]interface{}{
		"plate": ctx.String("plate"),
		"model": ctx.String("model"),
		"year":  strconv.Itoa(ctx.Int("year")),
	}
	fmt.Println(data)

	err := c.sendData(data)

	if err != nil {
		return err
	}
	return nil
}

func (c *CLIHandler) GetCars(ctx *cli.Context) error {
	var data Data
	data.Command = "getCars"
	data.Args = map[string]interface{}{}

	err := c.sendData(data)

	if err != nil {
		return err
	}
	return nil
}

func (c *CLIHandler) GetCar(ctx *cli.Context) error {
	var data Data
	data.Command = "getCar"
	data.Args = map[string]interface{}{
		"plate": ctx.String("plate"),
	}

	err := c.sendData(data)

	if err != nil {
		return err
	}
	return nil
}
